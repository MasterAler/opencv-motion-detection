#include <QCoreApplication>
#include <QCommandLineParser>
#include <QDebug>

#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/cvconfig.h>

const char cameraSource[] = "rtsp://10.0.61.214:8554/229";
const int EXIT_KEY = 113; // q key


int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    //------------ CMD -------------

    QCommandLineParser parser;

    QCommandLineOption sourceOption(QStringList() << "v" << "video", "path to the video file");
    parser.addOption(sourceOption);

    QCommandLineOption areaOption(QStringList() << "a" << "min-area", "minimum area size");
    parser.addOption(areaOption);

    parser.process(app);

//    cv::VideoCapture videoSource = parser.isSet(sourceOption)
//                                        ? cv::VideoCapture(parser.value(sourceOption).toLatin1().data())
//                                        : cv::VideoCapture(0);

//    int min_area = parser.isSet(areaOption)
//            ? parser.value(areaOption).toInt()
//            : 700;

    cv::VideoCapture videoSource = cv::VideoCapture(cameraSource);
    int min_area = 700;

    //--------------------

    if (!videoSource.isOpened())
    {
        std::cerr << "Source was not opened succesfully" << std::endl;
        return -1;
    }

    cv::Mat frame;
    cv::UMat bgmask, gray;
    const char* WIN_LIVE   = "LIVE";
    const char* WIN_MAGIC  = "MAGIC";

    cv::namedWindow(WIN_LIVE, cv::WINDOW_NORMAL);
    cv::namedWindow(WIN_MAGIC, cv::WINDOW_NORMAL);

    cv::Ptr<cv::BackgroundSubtractorMOG2> bgr = cv::createBackgroundSubtractorMOG2(500, 25, false);
    std::vector<std::vector<cv::Point>> contours;

    while (true)
    {
        if (!videoSource.read(frame))
        {
            std::cerr << "Broken frame!" << std::endl;
            break;
        }
        else
        {
            QString text("Unoccupied");
            cv::resize(frame, frame, cv::Size(frame.cols / 2, frame.rows / 2), 0, 0, cv::INTER_CUBIC);

            cv::cvtColor(frame, gray, cv::COLOR_BGR2GRAY);
            cv::GaussianBlur(gray, gray, cv::Size(21, 21), 0);

            bgr->apply(gray, bgmask, -1);
            cv::dilate(bgmask, bgmask, cv::Mat(), cv::Point(-1, -1), 3);

            cv::findContours(bgmask, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

            for(auto c = begin(contours); c != end(contours); ++c)
            {
                if (cv::contourArea(*c) < min_area)
                    continue;

                cv::Rect rect = cv::boundingRect(*c);
                cv::rectangle(frame, rect, cv::Scalar(0, 255, 0), 2);

                text = "Occupied";
            }

            cv::putText(frame, QString("Room Status: %1").arg(text).toLatin1().data()
                        , cv::Point(10, 20), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 0, 255), 2);

            cv::imshow(WIN_LIVE, frame);
            cv::imshow(WIN_MAGIC, bgmask);
        }

        // "q" key
        int key = cv::waitKey(1) & 0xFF;
        if (EXIT_KEY == key)
            break;
    }

    videoSource.release();
    cv::destroyAllWindows();

    return app.exec();
}
