QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

SOURCES += \
        main.cpp

win32 {
 OPENCV_DIR = D:\handmade_opencv4

 LIBS += -L$$OPENCV_DIR\x86\vc14\lib

 CONFIG(debug, debug|release) {
    LIBS +=-lopencv_world400d
 }else {
    LIBS +=-lopencv_world400
 }

 INCLUDEPATH *= $$OPENCV_DIR/include
}

unix {
 OPENCV_DIR = /home/aler/handmade_opencv_linux

 LIBS += -L$$OPENCV_DIR/lib \
        -lopencv_world

 INCLUDEPATH *= $$OPENCV_DIR/include/opencv4
}

DEPENDPATH *= $$INCLUDEPATH

DESTDIR = $$PWD/bin

